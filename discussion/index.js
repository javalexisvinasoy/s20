// console.log("Hello!");

// While Loop
/*
	- Takes a single condition. If the condition is true, it will run the code.

	Syntax:
		while(condition){
			statement
		}
*/

let count = 5;

while(count != 0){
	console.log("While: " + count);
	count --; // it will loop until it reaches 0
};

let num = 0;

while(num <= 5){
	console.log("While: " + num);
	num++; // repeats loop until num is equals to 5
}

// MINI ACTIVITY 1
// The while loop should only display even number/ incriment by 2.

let numA = 0;

while(numA <= 30) {
	console.log("While: " + numA)
	numA +=2;
}



// Do While Loop
/*
	- a do while loop works a lot like the while loop
	- but iunlike while loops, do-while loops guarantee that the code will be executed atleast once.

	Syntax:
		do {
			statement
		} while (expression/condition)
*/

let number = Number(prompt("Give me a number:"));

do {
	console.log("Do while: " + number);
	number++;
} while (number < 10)


number = Number(prompt("Give me another number:"));

do {
	console.log("Do while: " + number)
	number--
} while (number > 10)


// For loop
/*
	-  for loop is more flexible than while loop and do-while loop

	Parts:
		1. initial value: tracks the progress of loop
		2. condition: if the statement is true, then it will run a code; but if false, it will stop the iteration/code
		3. iteration (final expression): ir indicates how to advance the loop, whether it is increasing or decreasing 

	Syntax:
		for(initialValue; condition; iteration) {
			statement
		}

*/

	for(let count = 0; count <= 20; count++) {
		console.log("For loop count: " + count);
	}

	let myString = "rupert ramos"
	console.log(myString.length); // display the number or characters ; 12

	console.log(" ");
	console.log(myString[0]);
	console.log(myString[10]);

	for(let x = 0; x < myString.length; x++){
		console.log(myString[x])
	}

console.log(" ")

	let myName = "JOSEPHINE";

	for(let n = 0; n < myName.length; n++) {

		if(

			myName[n].toLowerCase() == "a" || 
			myName[n].toLowerCase() == "e" || 
			myName[n].toLowerCase() == "i" || 
			myName[n].toLowerCase() == "o" || 
			myName[n].toLowerCase() == "u"  
		){
			console.log("Vowel")
		} else {
			console.log(myName[n].toLowerCase())
		}
	}

// MINI ACTIVITY 2

let theWord = "extravagant"
let consonants = ""

for(let w = 0; w < theWord.length; w++) {

	if(
		theWord[w].toLowerCase() == "a" ||
		theWord[w].toLowerCase() == "e" ||
		theWord[w].toLowerCase() == "i" ||
		theWord[w].toLowerCase() == "o" ||
		theWord[w].toLowerCase() == "u" 
	) {

		continue;

	} else {
		consonants += theWord[w]
	}
}

console.log("The consonants are: " + consonants)


// Continue and Break Statements
/*
	"continue" statements allows the code to go the next iteration without finishing the execution of all the statements in the code block.

	"break" statement on the other hand is a keyword that ends the execution of the code or the current loop.
*/

for (let count = 1; count <= 20; count++) {
	if (count % 5 === 0){
		console.log("Div by 5")
		continue;
	} 

	console.log("Continue and Break: " + count);
	if (count > 10) {
		break;
	}
}


console.log(" ")

let name = "Alexander"

for(let i=0; i < name.length; i++) {
	console.log(name[i])

	if(name[i].toLowerCase() === "a"){
		console.log("Continue to the iteration.")
		continue;
	}

	if (name[i].toLowerCase() === "d") {
		console.log("Continue to the iteration.")
		break;
	}
}